import { useRef, useState } from "react";
import axios from 'axios';

export const Upload = () => {
    const [imgSrc, setImgSrc] = useState('');
    const [progress, setProgress] = useState(0);

    const inputRef = useRef();

    const handleThumbnail = () => {
        if (!inputRef.current.files.length) { return };
        const file = inputRef.current.files[0];

        const reader = new FileReader();

        reader.onloadend = () => {
            setImgSrc(reader.result);
        }

        reader.readAsDataURL(file);
    }

    const handleSubmit = () => {
        if (!inputRef.current.files.length) { return };
        const file = inputRef.current.files[0];

        const formData = new FormData();
        formData.append('image', file);

        axios.post('http://localhost:4040/files', formData, {
            onUploadProgress: (step) => {
                setProgress(step.progress);
            },
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((resp) => {
            console.log(resp.data);
        })
    }


    return (
        <div>
            <input onChange={handleThumbnail} type="file" name="image" ref={inputRef} />
            <button onClick={handleSubmit}>upload</button>
            {imgSrc && <img style={{
                width: '300px',
                opacity: progress
            }} src={imgSrc} alt="" />}
        </div>
    )
}

//upload a file using Axios