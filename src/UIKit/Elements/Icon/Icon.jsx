import './Icon.css';

export const Icon = ({ i }) => {
    return (
        <div className="Icon" data-testid="icon">
            <span className="material-symbols-outlined">
                {i}
            </span>
        </div>
    )
}