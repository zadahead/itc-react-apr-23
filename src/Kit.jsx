
import { Btn, Icon, Line, Between, Rows } from 'UIKit';

export const Kit = () => {
    const handleClick = () => {
        console.log('clicked')
    }
    return (
        <Rows>
            <h1>Kit</h1>
            <Between>
                <Line>
                    <div>pic</div>
                    <Rows>
                        <div>top section</div>
                        <Line>
                            <div>ZadaheaD</div>
                            <div>info</div>
                        </Line>
                    </Rows>
                </Line>
                <div>
                    <Btn onClick={handleClick}>click me</Btn>
                </div>
            </Between>

            <Rows>
                <Btn onClick={handleClick} i="search">click me</Btn>
                <Btn onClick={handleClick} i="settings">click me</Btn>
                <Btn onClick={handleClick}>click me</Btn>
            </Rows>

            <div>
                <Icon i="search" />
                <Icon i="settings" />
            </div>
        </Rows>
    )
}