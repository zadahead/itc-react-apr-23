export const calc = (a, b) => {
    if (
        typeof (a) === 'string' |
        typeof (b) === 'string'
    ) { return 0 }

    return Math.abs(a) + Math.abs(b);
}