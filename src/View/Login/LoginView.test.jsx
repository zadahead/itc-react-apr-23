import { fireEvent, render, screen } from "@testing-library/react";
import { LoginView } from "./LoginView"
import "@testing-library/jest-dom";

describe("<LoginView />", () => {
    it('will render the view with empty fields and disabled button', () => {
        const { username, password, btn } = setUp();

        expect(username.value).toEqual('');
        expect(password.value).toEqual('');
        expect(btn).toBeDisabled();
    })

    it('will enable the button if both inputs are filled', () => {
        const { username, password, btn } = setUp();

        fireEvent.change(username, { target: { value: 'user' } });
        fireEvent.change(password, { target: { value: 'pass' } });

        expect(btn).not.toBeDisabled();
    })

    it('will disable the button if one input is empty', () => {
        const { username, password, btn } = setUp();

        fireEvent.change(username, { target: { value: 'user' } });
        expect(btn).toBeDisabled();

        fireEvent.change(username, { target: { value: '' } });
        fireEvent.change(password, { target: { value: 'pass' } });
        expect(btn).toBeDisabled();
    })
})

const setUp = () => {
    render(<LoginView />)

    const username = screen.getByTestId('username');
    const password = screen.getByTestId('password');
    const btn = screen.getByTestId('btn');

    return { username, password, btn }
}