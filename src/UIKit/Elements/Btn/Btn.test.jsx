/* eslint-disable testing-library/no-node-access */
import { render, screen, fireEvent } from '@testing-library/react';
import "@testing-library/jest-dom";

import { Btn } from './index';

describe('<Btn />', () => {
    it('will render a button element', () => {
        render(<Btn />)
        const btn = screen.getByRole('button');

        expect(btn).toBeInTheDocument();
    })

    it('will render children inside .text class', () => {
        const children = "Click me";

        render(<Btn>{children}</Btn>)

        const text = screen.getByTestId('text-wrap');
        expect(text).toBeInTheDocument();

        expect(text.innerHTML).toEqual(children);
    })

    it('will render with an icon', () => {
        const i = "search"
        render(<Btn i={i}>Click me</Btn>);

        const icon = screen.queryByTestId('icon');
        expect(icon).toBeInTheDocument();
        expect(icon.children.length).toBe(1);

        expect(icon.children[0].innerHTML).toEqual(i);
    })

    it('will not render icon', () => {
        render(<Btn>Click me</Btn>);
        const icon = screen.queryByTestId('icon');
        expect(icon).not.toBeInTheDocument();
    })

    it('will trigger a function', () => {
        const handleClick = jest.fn();

        render(<Btn onClick={handleClick}>Click me</Btn>);

        const btn = screen.getByTestId('btn');
        fireEvent.click(btn);

        expect(handleClick).toBeCalledTimes(1);
    })
})