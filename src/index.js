import React from 'react';
import ReactDOM from 'react-dom/client';
import { App } from './App';

import "./index.css";
import { Btn } from 'UIKit';
import { Login } from 'Login';
import { LoginView } from 'View/Login/LoginView';

const handleClick = () => {
  console.log('Clicked');
}
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <LoginView />
);

