
import { calc } from './intro';

describe("Validating calc function", () => {
    it("should add correctly to 25", () => {
        const result = calc(10, 15);
        expect(result).toBe(25);
    })

    it("should sum with value differant then 25", () => {
        const result = calc(25, 15);
        expect(result).toBe(40);
    })
    it('show add correctly with negative numbers', () => {
        const result = calc(-5, -5);
        expect(result).toBe(10)
    })
    it('should return XXX if string is added', () => {
        const result = calc("5", 10);
        expect(result).toBe(0);
    })
})