describe("This is our first cypress test", () => {
    it("will verify btn is disabled and input are empty", () => {
        cy.visit("http://localhost:3000/");

        cy.get('input[data-testid="username"]').should('have.value', '');
        cy.get('input[data-testid="password"]').should('have.value', '');

        cy.get('[data-testid="btn"]').should('be.disabled');
    })

    it('will fill up both inputs, and verify button is enabled', () => {
        cy.visit("http://localhost:3000/");

        cy.get('input[data-testid="username"]').type('user');
        cy.get('input[data-testid="password"]').type('pass');

        cy.get('[data-testid="btn"]').should('be.enabled');
    })
})