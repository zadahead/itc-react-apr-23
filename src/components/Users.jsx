import { useEffect, useRef, useState } from "react"
import { GET } from "../utils/api"

import './Users.css';

export const Users = () => {
    const [q, setQ] = useState('');
    const [list, setList] = useState(null);
    const [sortDirection, setSortDirection] = useState(1);
    const [skip, setSkip] = useState(0);
    const [limit, setLimit] = useState(5);
    const timer = useRef();

    useEffect(() => {
        /*
            query
            sort
            limit 
            paging 
        */
        timer.current = setTimeout(() => {
            GET(`/users/search`, {
                q,
                sort: 'email',
                direction: sortDirection,
                limit,
                skip
            }).then(list => {
                setList(list);
            })
        }, 700);

        return () => {
            clearTimeout(timer.current);
        }

    }, [q, sortDirection, skip, limit])


    return (
        <div className="Users">
            <div>
                <input placeholder="search" onChange={(e) => setQ(e.target.value)} />
                <input placeholder="skip" value={skip} onChange={(e) => setSkip(e.target.value)} />
                <input placeholder="limit" value={limit} onChange={(e) => setLimit(e.target.value)} />
                <div onClick={() => setSortDirection(-1)}>sort email desc</div>
                <div onClick={() => setSortDirection(1)}>sort email asc</div>
            </div>
            <div>
                {list && list.map(i => <h3 key={i._id}>{`name: ${i.name}, age: ${i.age}, email: ${i.email}`}</h3>)}
            </div>
        </div>
    )
}