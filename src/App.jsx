
import "./App.css";
import { Kit } from "./Kit";

export const App = () => {

    return (
        <div className="App">
            <Kit />
        </div>
    )
}