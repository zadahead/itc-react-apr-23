import { Between, Icon } from 'UIKit';
import './Btn.css';

export const Btn = ({ onClick, children, i, disabled }) => {

    return (
        <button className="Btn" onClick={onClick} data-testid="btn" disabled={disabled}>
            <Between>
                <div className='text' data-testid="text-wrap" >
                    {children}
                </div>
                {i && <Icon i={i} />}
            </Between>
        </button>
    )
}