import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:2500'
});

const getHeaders = () => {
    return {
        headers: {
            'Authorization': `${localStorage.getItem("USER")}`
        }
    }
}


export const GET = async (url, params = {}) => {
    try {
        const resp = await api.get(url, { ...getHeaders(), params });
        return resp.data;
    } catch (error) {
        handleError(error);
    }
}

export const POST = async (url, body) => {
    try {
        const resp = await api.post(url, body, getHeaders());
        return resp.data;
    } catch (error) {
        handleError(error);
    }
}

// export const POST = (url, body) => {
//     return new Promise(async (res, rej) => {
//         try {
//             const resp = await api.post(url, body);
//             res(resp.data);
//         } catch (error) {
//             handleError(error);
//            // rej(); //this is the clue
//         }
//     })
// }


const handleError = (error) => {
    if (typeof (error) == 'string') {
        alert(error)
    } else if (error.response) {
        alert(error.response.data.message);
    } else {
        alert(error.message);
    }

}
