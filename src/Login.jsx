import { useEffect, useState } from "react"
import { POST, GET } from "./utils/api";
import { Users } from "./components/Users";

/*
    1) create a new route for login
    2) call axios with email & password from the client 
    3) validate user -> 
        if valid => res status 200
        else => res status 401
*/

export const Login = () => {
    const [user, setUser] = useState(null);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        loadMe();
    }, [])

    const loadMe = () => {
        GET('/users/me')
            .then(resp => {
                setUser(resp);
            })
    }

    const handleLogin = () => {
        POST('/auth/login', { email, password })
            .then((resp) => {
                console.log(resp);
                localStorage.setItem("USER", resp.accessToken);
                //loadMe();
            })
    }


    return (
        <div>
            {
                user && (
                    <div>
                        <h2>{user.name}</h2>
                        <h4>{user.email}</h4>
                    </div>
                )
            }
            <input placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <input placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            <button onClick={handleLogin}>login</button>

            <Users />
        </div>
    )
}