import { Btn, Rows } from "UIKit"
import { useState } from "react"

export const LoginView = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const handleLogin = () => {
        console.log('login', username, password);
    }

    const isDisabled = !username || !password;

    return (
        <Rows>
            <input data-testid="username" value={username} onChange={(e) => setUsername(e.target.value)} />
            <input data-testid="password" value={password} onChange={(e) => setPassword(e.target.value)} />

            <Btn onClick={handleLogin} disabled={isDisabled}>Login</Btn>
        </Rows>
    )
}

/*
    1) 
        - make sure username is empty
        - make sure password is empty
        - make sure button is disabled

    2) 
        - set value to username
        - set value to password
        - make sure button is enabled

    3) 
        - set only one of the inputs 
        - make sure button is disabled
        - set only the other input
        - make sure button is still disables
*/